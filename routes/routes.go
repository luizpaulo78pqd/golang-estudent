package routes

import (
	"fmt"
	"golang-estudent/controllers/books"

	"github.com/gin-gonic/gin"
)

func Routes() {
	router := gin.New()
	router.Use(gin.Recovery())

	router.GET("/albums", books.GetAlbums)
	router.POST("/albums", books.GetAlbums)
	router.GET("/albums/:id", books.GetAlbumByID)

	var err error

	if err != nil {
		fmt.Println(err)
	}

	router.Run("localhost:8080")
}
